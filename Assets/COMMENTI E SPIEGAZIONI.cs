﻿/* ABILITA'
   le abilità sono contenute in un GameObject e possiedono: 
   -un AbilityGenericScript
   -uno script diverso per ogni abilità che è l'implementazione della classe astratta LinkedEffect
   -nessuno, uno o più script che sono l'implementazione della classe astratta ResourceCost
   gli script delle abilità ereditano tutti dalla calsse astratta LinkedEffect ( nomescript : LinkedEffect) e effettuano l'override del metodo ApplyEffect, il resto dell implementazione non ha importanza
   il metodo ApplEffect può restituire o null o 1 o più valori reali inseriti in un array, il significato della posizione dei valori va specificato nella documentazione dello script stesso

   benche tutte le abilità hanno una loro implementazione generale alcune devono permettere il salvataggio di valori specifici che dipendono dall istanza stessa dell abilità
   (per esempio considerando l'attacco base di un unità un abilità ogni unità deve avere i suoi parametri specifici dell attacco contenuti nell abilità stessa)
   le abilità di questo tipo sono contenute in GameObject che possiedono
   -un AbilitySpecificScript
   -uno script diverso per ogni abilità (ma comune a tutte le istanze della stessa, anche se i valori dei parametri variano) contenente le variabili legate all istanza dell abilità
   tramite questo GameObject è possibile quindi richiamare la funzione generale della classe passando a ApplyEffect come ultimo parametro lo script contenete i valori legati all istanza dell abilità

   la classe static AbilityApplier implementa le funzioni che permettono di gestire questi due diversi GameObject come se fossero tutti dello stesso tipo in maniera automatica
   è quindi consigliato utilizzare questa classe, al posto di accedere manualmente alle abilità, anche se è comunque possibile farlo
 */