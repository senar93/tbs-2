﻿/* autore: Andrea Teruzzi
 */
using UnityEngine;
using System.Collections;




public class AbilityGenericScript : MonoBehaviour {
	public bool passive;

	public bool visible;
	public bool canBeDisabled, canBeDispelled;
	public int priority = 0;                        //effetti con priorità maggiore sono eseguiti prima

	//public EffectTagTypeEnum effectType;
	public EffectApplicationTagTypeEnum applicationTag;
	public LinkedEffect linkedScript;

	public ResourceCost[] resourceCostScript = new ResourceCost[1];


	public bool HaveResourceCost() {
		if (resourceCostScript == null)
			return false;
		return true;
	}

}
