﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public abstract class BattleObjectSingleResourceRegen
{
	/* classe astratta che implementa la rigenerazione di una statistica generica
	 * quando richiamato il metodo Regen implementato dalle varie classi deve rigenerare la risorsa specificata
	 */
	public abstract void Regen();
}