﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public abstract class BattleRoundSystem : MonoBehaviour
{
    /* classe astratta che permette di implementare i vari sistemi di calcolo dei turni, 
     * 
     */
    abstract public List<GameObject> GroupRoundOrder(List<GameObject> groupList, BattleTurnTagType resourceTag);

}