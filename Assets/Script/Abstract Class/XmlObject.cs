﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


public abstract class XmlRoot<T> {
	[XmlAttribute("LoadFromString")] public bool loadFromString;
	[XmlAttribute("FileVersion")] public string fileVersion;
	[XmlAttribute("path")] public string path;

	virtual public void Save() {
		var serializer = new XmlSerializer(typeof(T));
		using (var stream = new FileStream(path, FileMode.Create))
			serializer.Serialize(stream, this);
	}

	virtual public T Load(string rowString = "") {
		var serializer = new XmlSerializer(typeof(T));
		if (rowString != "")		//legge il file dalla stringa passata come parametro (da usare insieme al tipo WWW)
			return (T)serializer.Deserialize(new StringReader(rowString));
		else						//Load from xml file
			using (var stream = new FileStream(path, FileMode.Open))
				return (T)serializer.Deserialize(stream);
	}

}


/*
// Exemple
[System.Serializable]
[XmlRoot("Container")]
public class TestContainer : XmlRoot<TestItem> {			// Root
	[XmlArray("Items"), XmlArrayItem("Item")]
	public TestItem[] item;
}


[System.Serializable]
public class TestItem									// Leaf
{
	[XmlAttribute("nome")]
	public string name;

	public int num;
	public AttackGeneric att;
}
*/
