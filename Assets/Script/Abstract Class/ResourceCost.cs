﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public abstract class ResourceCost : MonoBehaviour
{
    virtual public ResourceCostTypeEnum[] GetResourceType() { return null; }
    virtual public float[] GetResourceCost() { return null; }
}
