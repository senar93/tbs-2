﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public abstract class LinkedEffect : MonoBehaviour
{
    /* classe astratta che implementa un generico effetto di un abilità, può ritornare un valore e/o compiere un azione su un target
	 * l'azione da compiere deve essere scritta come implementazione del metodo ApplyEffect(...), e può essere qualsiasi cosa
	 * le funzioni che usano linkedSpecificValue sono quelle abilità che basano il proprio comportamento su qualche parametro specifico dell istanza in cui vengono richiamate
	 */
    virtual public float[] ApplyEffect(GameObject[] target = null,                  //tutti i target dell abilità
                                       GameObject sender = null,                        //utilizatore/possessore dell abilità (oggetto che la attiva) 
                                       ResourceCost[] linkedResourceCost = null,    //costi in risorse dell abilità
                                       MonoBehaviour linkedSpecificValue = null,    //valori specifici dell istanza dell abilità
                                       string optionalParameters = ""               //stringa contenente parametri opzionali, il significato è scritto nella documentazione di ogni singola funzione
                                       )
    { return null; }                      //i 5 parametri potrebbero puntare, tutti o alcuni, a null
                                          //la funzione restituisce un array di valori, il significato dei valori in base alla posizione è scritto nella funzione stessa

    //aggiungere altri overide per la funzione ApplyEffect(...) in modo da coprire tutti i possibili target
}
