﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public abstract class BattleObject : MonoBehaviour
{
	//classe astratta che implementa un generico oggetto presente in battaglia
	public BattleObjectAbility ability;
	public BattleObjectHealth health;
	public BattleObjectResource resource;

	public GameObject owner;

	//funzione che setta i parametri specificati nell oggetto (NON è il costruttore e non imposta i parametri ereditati)
	public void ConstructorBattleObject(List<GameObject> tmpAbilityList, GameObject tmpOwner = null)
	{
		owner = tmpOwner;
		ability = new BattleObjectAbility(tmpAbilityList, this.gameObject, true);
		
		//da implementare
		//funzione per settare i vari valori di resource, la lista dei tag viene presa da una funzione di ability

		health.value = new BattleObjectStat(EffectApplicationTagTypeEnum.healthBase, EffectApplicationTagTypeEnum.healthInc,
											EffectApplicationTagTypeEnum.healthIncXC, EffectApplicationTagTypeEnum.healthIncLast);
		health.value.Get_MaxStat(true, ability);
	}

}