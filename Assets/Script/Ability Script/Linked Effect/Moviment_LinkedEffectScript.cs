﻿using UnityEngine;
using System.Collections;

public class Moviment_LinkedEffectScript : LinkedEffect {

	public override float[] ApplyEffect(GameObject[] target = null, GameObject sender = null, ResourceCost[] linkedResourceCost = null, MonoBehaviour linkedSpecificValue = null, string optionalParameters = "")
	{
		float[] tmp = new float[3];
		tmp[0] = (float)linkedSpecificValue.GetComponent<Moviment_LinkedSpecificValueScript>().moviment.maxMovimentDistance;
		tmp[1] = (float)linkedSpecificValue.GetComponent<Moviment_LinkedSpecificValueScript>().moviment.movimentType;
		tmp[3] = (float)linkedSpecificValue.GetComponent<Moviment_LinkedSpecificValueScript>().moviment.movimentInitiativeCost;
		return tmp;
	}

}
