﻿using UnityEngine;
using System.Collections;

public class HealthIncLast_LinkedEffectScript : LinkedEffect
{
	//incrementa gli hp del gruppo direttamente dentro la funzione, e restituisce l'incremento
	public override float[] ApplyEffect(GameObject[] target = null, GameObject sender = null, ResourceCost[] linkedResourceCost = null, MonoBehaviour linkedSpecificValue = null, string optionalParameters = "")
	{
		float[] tmp = new float[1];

		tmp[0] = linkedSpecificValue.GetComponent<HealthIncGeneric_LinkedSpecificValueScript>().healthInc;

		//incrementa gli hp
		sender.GetComponent<BattleObject>().health.value.Set_MaxStat(
				linkedSpecificValue.GetComponent<HealthIncGeneric_LinkedSpecificValueScript>().healthInc
				+ sender.GetComponent<BattleObject>().health.value.Get_MaxStat());

		return tmp;
	}
}
