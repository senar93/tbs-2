﻿using UnityEngine;
using System.Collections;

public class Health_LinkedEffectScript : LinkedEffect {
	public override float[] ApplyEffect(GameObject[] target = null, GameObject sender = null, ResourceCost[] linkedResourceCost = null, MonoBehaviour linkedSpecificValue = null, string optionalParameters = "")
	{
		float[] tmp = new float[1];
		tmp[0] = (float)linkedSpecificValue.GetComponent<Health_LinkedSpecificValueScript>().health.health;
		return tmp;
	}
}
