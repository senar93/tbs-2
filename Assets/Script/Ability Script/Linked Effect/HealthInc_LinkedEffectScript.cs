﻿using UnityEngine;
using System.Collections;

public class HealthInc_LinkedEffectScript : LinkedEffect
{
	public override float[] ApplyEffect(GameObject[] target = null, GameObject sender = null, ResourceCost[] linkedResourceCost = null, MonoBehaviour linkedSpecificValue = null, string optionalParameters = "")
	{
		float[] tmp = new float[1];
		tmp[0] = linkedSpecificValue.GetComponent<HealthIncGeneric_LinkedSpecificValueScript>().healthInc;
		return tmp;
	}
}
