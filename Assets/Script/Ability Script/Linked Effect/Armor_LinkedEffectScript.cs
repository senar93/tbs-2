﻿using UnityEngine;
using System.Collections;

public class Armor_LinkedEffectScript : LinkedEffect {

	public override float[] ApplyEffect(GameObject[] target = null, GameObject sender = null, ResourceCost[] linkedResourceCost = null, MonoBehaviour linkedSpecificValue = null, string optionalParameters = "")
	{
		float[] tmp = new float[2];
		tmp[0] =(float) linkedSpecificValue.GetComponent<Armor_LinkedSpecificValueScript>().armor.armorValue;
		tmp[1] =(float) linkedSpecificValue.GetComponent<Armor_LinkedSpecificValueScript>().armor.armorValue;
		return tmp;
	}

}
