﻿/* autore: Andrea Teruzzi
 */
using UnityEngine;
using System.Collections;

public class Attack_LinkedEffectScript : LinkedEffect {

	public override float[] ApplyEffect(GameObject[] target = null, GameObject sender = null, ResourceCost[] linkedResourceCost = null, MonoBehaviour linkedSpecificValue = null, string optionalParameters = "")
	{
		float[] tmp = new float[6];
		tmp[0] =(float) linkedSpecificValue.GetComponent<Attack_LinkedSpecificValueScript>().attack.damageValue;
		tmp[1] =(float) linkedSpecificValue.GetComponent<Attack_LinkedSpecificValueScript>().attack.damageType;
		tmp[3] =(float) linkedSpecificValue.GetComponent<Attack_LinkedSpecificValueScript>().attack.attackType;
		tmp[4] =(float) linkedSpecificValue.GetComponent<Attack_LinkedSpecificValueScript>().attack.rangeValueMin;
		tmp[5] =(float)linkedSpecificValue.GetComponent<Attack_LinkedSpecificValueScript>().attack.rangeValueMax;
		tmp[6] =(float) linkedSpecificValue.GetComponent<Attack_LinkedSpecificValueScript>().attack.attackInitiativeCost;
		return tmp;
	}

}
