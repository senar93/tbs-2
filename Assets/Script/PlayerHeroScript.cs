﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class PlayerHeroScript : MonoBehaviour {
	//l'indice indica la modalità di gioco
	static int[] MAX_UNITY_POINT = { 100 };						//usati per inserire unità e ausiliari
	static int[] MAX_HERO_POINT = { 100 };						//usati per incrementare il livello del generale, comprare abilità o oggetti, ecc
	static int[] MAX_GENERIC_POINT = { 100 };					//usati in aggiunta ai precedenti
	static int GROUP_SLOT_LIMIT = 7;
	static int MAX_LEVEL = 30;
	static int[] POINT_TO_NEXT_LEVEL = new int[/* MAX_LEVEL-1 */] {
		1,		2,		3,		4,		5,	
		6,		7,		8,		9,		10,
		11,		12,		13,		14,		15,
		16,		17,		18,		19,		20,
		21,		22,		23,		24,		25,
		26,		27,		28,		29
	};



	public GameObject race;										//da qui prende l'albero dei talenti, unità e ausiliari, abilità, ecc
	public int actLevel;
	public int actUnityPoint = 0, actHeroPoint = 0, actGenericPoint = 0;
	public List<GameObject> ability;                            //lista abilità (non tutte, solo le abilità acquistate come tali)
	public List<GameObject> talent;								//lista talenti sbloccati
	public List<GameObject> item;								//lista oggetti posseduti
	public List<List<GameObject>> army;                         //lista unità esercito, l'elemento 0 puntato dal secondo indice è l'unità, gli altri i suoi ausiliari
	public List<int> armyNumber;								//lista numero di unità nell esercito
	public List<List<GameObject>> specialUnity;                 //come la variabile precedente, contiene unità speciali che non sono da considerarsi esattametne come gruppi normali
	public List<int> specialUnityNumber;
	public int heroBoundAtUnity = -1;                           //indica se l'eroe è da considerare come elemento a parte in battaglia o legato a un patricolare gruppo (-1 = elemento a parte, altrimenti contiene l'indice di army)

	
}
