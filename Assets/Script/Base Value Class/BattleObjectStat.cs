﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class BattleObjectStat
{
	public bool haveStat { get; private set; }          //variabile di sola lettura all esterno, indica se l'oggetto posside la statistica o no

	private float maxStat = 0;                          //contiene la statistica massima del oggetto dopo il primo calcolo di Get_MaxStat richiamato con forceRecalculate = true
	public EffectApplicationTagTypeEnum statBase;
	public EffectApplicationTagTypeEnum statInc;
	public EffectApplicationTagTypeEnum statIncXC;
	public EffectApplicationTagTypeEnum statIncLast;

	public BattleObjectStat(EffectApplicationTagTypeEnum tmpStatBase, EffectApplicationTagTypeEnum tmpStatInc,
							EffectApplicationTagTypeEnum tmpStatIncXC, EffectApplicationTagTypeEnum tmpStatIncLast/*, BattleObjectAbility ability,*/)
	{
		statBase = tmpStatBase;
		statInc = tmpStatInc;
		statIncXC = tmpStatIncXC;
		statIncLast = tmpStatIncLast;
		//Get_MaxStat(true, ability);		//BUG non calcola l'incremento con il tag *Last se richiamata nel costruttore
											//richiamandola in qualsiasi altro metodo del programma il calcolo viene effettuato correttamente
	}

	//imposta maxStat al valore passato come parametro
	//DA UTILIZARE CON CAUTELA
	//es. viene utilizata nelle funzioni con tag HealthIncLast poichè il calcolo degli hp è completamente in mano loro e devono reimpostare da 0 il parametro
	public void Set_MaxStat(float newValue)
	{
		maxStat = newValue;
	}

	//restituisce la statistica massima della singola unità
	//se forceRecalculate è true, ricalcola da 0 la statistica, altrimenti restituisce il valore del calcolo precedente
	//se ability = null viene restituito il valore di maxStat del calcolo precedente
	//se non c'è stato alcun calcolo precedente, e forceRecalculate = false maxStat sarà 0
	//durante il calcolo vengono richiamate in ordine CalcBase, CalcInc, CalcIncXC e CalcIncLast
	public float Get_MaxStat(bool forceRecalculate = false, BattleObjectAbility ability = null)
	{
		if (forceRecalculate)
			if (ability != null)
			{
				GameObject objTarget = ability.GetObjOwner();
				CalcBase(objTarget, ability.abilityList);
				if (ability.abilityList != null && objTarget != null)
				{
					CalcInc(objTarget, ability.abilityList);
					CalcIncXC(objTarget, ability.abilityList);
					CalcIncLast(objTarget, ability.abilityList);
				}

			}

		return maxStat;
	}



	//Setta maxStat al valore contenuto nel abilità con tag(EffectTagTypeEnum) health
	//se questa abilità non è presente imposta maxStat a 0 e la variabile haveStat a false
	private void CalcBase(GameObject objTarget, List<GameObject> tmpList)
	{
		List<GameObject> tmpPartialList = BattleObjectAbility
										  .GetAbilityByeApplicationTag(tmpList, statBase, true);
		if (tmpPartialList != null)
		{
			maxStat = BattleObjectAbility.ApplyEffect(tmpPartialList[0], null, objTarget)[0];
			haveStat = true;
		}
		else {
			maxStat = 0;
			haveStat = false;
		}
	}

	//calcola l'incremento fisso
	//tmpPartiaList è ordinata per priorità in ordine decrescente (viengono eseguite prima le abilità con priority più alta)
	private void CalcInc(GameObject objTarget, List<GameObject> tmpList)
	{
		//calcolo inc fisso
		List<GameObject> tmpPartialList = BattleObjectAbility
										  .GetAbilityByeApplicationTag(tmpList, statInc, true);
		for (int i = 0; tmpPartialList != null && i < tmpPartialList.Count; i++)
			maxStat += BattleObjectAbility.ApplyEffect(tmpPartialList[i], null, objTarget)[0];
	}

	//calcolo inc percentuale
	// il calcolo è del tipo "(base*inc1)*inc2" per inc con priorità diversa
	// e "base*(inc1+inc2)" per inc con priorità uguale
	// tmpPartiaList è ordinata per priorità in ordine decrescente (viengono eseguite prima le abilità con priority più alta)
	private void CalcIncXC(GameObject objTarget, List<GameObject> tmpList)
	{
		List<GameObject> tmpPartialList = BattleObjectAbility
										  .GetAbilityByeApplicationTag(tmpList, statIncXC, true);
		int previusPriority = 0;
		bool previusPriortySet = false;
		float tmpTotInc = 0;
		for (int i = 0; tmpPartialList != null && i < tmpPartialList.Count; i++)
		{
			if (!previusPriortySet)
			{
				//inizializza la priorità precedente alla prima iterazione del ciclo
				previusPriority = BattleObjectAbility.GetPriority(tmpPartialList[i]);
				previusPriortySet = true;
			}

			if (BattleObjectAbility.GetPriority(tmpPartialList[i]) == previusPriority)
			{
				tmpTotInc += BattleObjectAbility.ApplyEffect(tmpPartialList[i], null, objTarget)[0] - 1;
			}
			else {
				maxStat *= tmpTotInc + 1;
				tmpTotInc = BattleObjectAbility.ApplyEffect(tmpPartialList[i], null, objTarget)[0] - 1;
			}
		}
		//calcola l'incremento dell ultima iterazione (che è contenuto in tempTotInc, e non è stato applicato)
		maxStat *= tmpTotInc + 1;
	}

	//calcolo finale
	//la gestione di questo calcolo è completamente in mano all abilità stessa, lo script si limita a richiamare ApplyEffect
	//tmpPartiaList è ordinata per priorità in ordine decrescente (viengono eseguite prima le abilità con priority più alta)
	private void CalcIncLast(GameObject objTarget, List<GameObject> tmpList)
	{
		List<GameObject> tmpPartialList = BattleObjectAbility
										  .GetAbilityByeApplicationTag(tmpList, statIncLast, true);
		for (int i = 0; tmpPartialList != null && i < tmpPartialList.Count; i++)
			BattleObjectAbility.ApplyEffect(tmpPartialList[i], null, objTarget);
	}
}