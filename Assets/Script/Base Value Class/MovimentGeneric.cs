﻿[System.Serializable]
public class MovimentGeneric
{
    static int MIN_MOVIMENT_DISTANCE_ALLOWED = 0;
    static float MIN_MOVIMENT_INITIATIVE_COST_ALLOWED = 0;

    public int maxMovimentDistance = 1;                     //distanza massima percorribile con un singolo comando di movimento
    public MovimentTypeEnum movimentType = MovimentTypeEnum.none;
    public float movimentInitiativeCost = 1f;               //iniziativa spesa per ogni casella percorsa
}