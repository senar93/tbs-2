﻿[System.Serializable]
public class StaminaGeneric
{
    static int MIN_STAMINA_ALLOWED = 0;

    //public bool haveStamina = false;
    public float stamina = 0;
    public float staminaRegen = 0;
}