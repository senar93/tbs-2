﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnitySource {

	public GameObject baseUnity;					//contiene l'unità di base del gruppo
	public List<GameObject> auxiliaryList;			//contiene la lista di ausiliari utilizzati dal gruppo

	private List<GameObject> AllAbility = null;


	//popola la lista AllAbility con le abilità degli ausiliari e dell unità
	//se AllAbility non è stato calcolato in precedenza (oppure se forceRecalculate = true) lo ricalcola
	//se AllAbility è stato calcolato in precedenza E forceRecalculate = false restituisce il valore di AllAbility del calcolo precedente
	//se toOrder = true ordina la lista AllAbility in ordine decrescente in base al parametro priorty
	public List<GameObject> Get_AllAbility(bool forceRecalculate = false, bool toOrder = false) {
		if (forceRecalculate || AllAbility == null) {
			AllAbility = new List<GameObject>(BaseUnityAbilityCount() + AuxiliaryAbilityCount());
			//add baseUnity ability
			if (baseUnity != null)
				AllAbility.AddRange(baseUnity.GetComponent<BaseUnityScript>().abilityList);
			//add auxiliary ability
			for (int i = 0; i < auxiliaryList.Count; i++)
				if (auxiliaryList[i] != null)
					AllAbility.AddRange(auxiliaryList[i].GetComponent<BaseAuxiliaryScript>().AbilityList);
			AbilityRedundantRemover(AllAbility);
		}

		if (toOrder)
			BattleObjectAbility.OrderByPriority(AllAbility);

		return AllAbility;
	}

	//rimuove eventuali abilità ridondanti
	private void AbilityRedundantRemover(List<GameObject> tmpList) {
		//da implementare
		;
	}

	private int BaseUnityAbilityCount()
	{
		if (baseUnity != null)
			return baseUnity.GetComponent<BaseUnityScript>().abilityList.Length;
		else
			return 0;
	}

	private int AuxiliaryAbilityCount()
	{
		int n = 0;
		for (int i = 0; i < auxiliaryList.Count; i++)
			if(auxiliaryList[i] != null)
				n += auxiliaryList[i].GetComponent<BaseAuxiliaryScript>().AbilityList.Length;
		return n;
	}
	


	//controlla se l'oggetto contiene un unità in baseUnity e degli ausiliari in auxiliaryList
	//NON controlla se quegli ausiliari e/o quel unità sono corretti per l'oggetto, ma solo se sono effettivamente ausialiari e unità 
	public bool Validate() {
		return true;
	}


}