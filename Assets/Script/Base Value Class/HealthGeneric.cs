﻿[System.Serializable]
public class HealthGeneric
{
    static int MIN_HEALTH_ALLOWED = 1;

    public int health = 1;
}