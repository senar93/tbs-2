﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BattleObjectHealth {
	public int unityNumber = 1;						//
	public float lastUnityHp = 1;                   //
	public BattleObjectStat value;	
}