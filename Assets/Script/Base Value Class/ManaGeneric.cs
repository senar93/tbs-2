﻿[System.Serializable]
public class ManaGeneric
{
    static int MIN_MANA_ALLOWED = 0;

    //public bool haveMana = false;
    public float mana = 0;
    public float manaRegen = 0;
}