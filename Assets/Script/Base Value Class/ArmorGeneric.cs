﻿[System.Serializable]
public class ArmorGeneric
{
    static int MIN_ARMOR_ALLOWED = 0;

    public int armorValue = 0;
    public ArmorTypeEnum armorType = ArmorTypeEnum.none;
}