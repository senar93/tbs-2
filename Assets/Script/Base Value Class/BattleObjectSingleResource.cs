﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BattleObjectSingleResource
{
	public float actValue;
	public BattleObjectStat resource;
	public BattleObjectSingleResourceRegen regen;
	public ResourceCostTypeEnum type;

	//costruttore, se specificValue != -1 assegna a actValue il valore specifico
	public BattleObjectSingleResource(ResourceCostTypeEnum tmpType, BattleObjectAbility ability, float specificValue = -1)
	{
		EffectApplicationTagTypeEnum[] tmpTag = BattleObjectSingleResource.FindTag_BattleObjectSingleResource(tmpType);
		if (tmpTag != null)
		{
			resource = new BattleObjectStat(tmpTag[0], tmpTag[1], tmpTag[2], tmpTag[3]);
			float tmpRes = resource.Get_MaxStat(true, ability);
			if (specificValue >= 0)
				actValue = specificValue;
			else
				actValue = tmpRes;
		}
	}



	//restituisce tutti i tag necessari il costruttore di BattleObjectSingleResource dato il tipo di risorsa
	public static EffectApplicationTagTypeEnum[] FindTag_BattleObjectSingleResource(ResourceCostTypeEnum type)
	{
		switch (type)
		{
			case ResourceCostTypeEnum.mana:
				return new EffectApplicationTagTypeEnum[] { EffectApplicationTagTypeEnum.manaBase,
															EffectApplicationTagTypeEnum.manaInc,
															EffectApplicationTagTypeEnum.manaIncXC,
															EffectApplicationTagTypeEnum.manaIncLast };
			case ResourceCostTypeEnum.stamina:
				return new EffectApplicationTagTypeEnum[] { EffectApplicationTagTypeEnum.staminaBase,
															EffectApplicationTagTypeEnum.staminaInc,
															EffectApplicationTagTypeEnum.staminaIncXC,
															EffectApplicationTagTypeEnum.staminaIncLast };
		}

		return null;
	}


}