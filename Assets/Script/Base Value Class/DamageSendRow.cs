﻿using UnityEngine;
using System.Collections.Generic;


[System.Serializable]
public class DamageSendRow
{
    /* contiene le informazioni necessarie per permettere a chi lo riceve, considerando le proprie abilità, la creazione dell oggetto DamageSendProcessed
	 */
    public GameObject target;                    //puntatore al gruppo che riceverà i danni
    public GameObject senderGroup;              //puntatore al gruppo che ha creato l'oggetto
    public GameObject senderAbility;            //puntatore all abilità che ha creato l'oggetto
    public List<GameObject> abilityStriker;     //contiene le abilità presenti sull attaccante che influenzano l'attacco (non tutte le abilità dell attaccante quindi)
    public List<GameObject> abilityReceiver;    //contiene le abilità presenti sull bersaglio dell attacco che influenzano l'attacco (non tutte le abilità dell bersaglio quindi)


    public DamageSendRow(GameObject target, GameObject tmpSenderGroup = null, GameObject tmpSenderAbility = null)
    {
        this.target = target;
        this.senderGroup = tmpSenderGroup;
        this.senderAbility = tmpSenderAbility;
        //funzione che filtrerà le abilità del attaccante in modo da inserire nella lista solo le abilità effettivamente usate nel calcolo dei danni
        this.abilityStriker = FilterAbilityStriker(senderGroup, senderAbility);
        //funzione che filtrerà le abilità del difensore in modo da inserire in questa lista solo le abilità effettivamente usate nel calcolo dei danni
        this.abilityReceiver = FilterAbilityReceiver(target, senderAbility);
    }

    public static List<GameObject> FilterAbilityStriker(GameObject senderGroup, GameObject senderAbility)
    {
        //da implementare
        return new List<GameObject>();
    }
    public static List<GameObject> FilterAbilityReceiver(GameObject target, GameObject senderAbility)
    {
        //da implementare
        return new List<GameObject>();
    }

}




/*
[System.Serializable]
public class DamageSendProcessed
{
    // oggetto contenente i danni gia pronti per essere applicati (quindi ottenuto dopo eventuali calcoli che coninvolgono le statistiche del gruppo attaccante e parte delle abilità del difensore
    // quando si arriva a utilizare questo oggetto non si ha più alcuna informazione sul oggetto che lo ha creato, si perdono quindi tutte le informazioni sul attaccante poiche a questo punto non sono significative
    // tutti i calcoli che riguardano in qualche modo l'attaccante devono essere completati prima della creazione di quest oggetto e ne vanno a influenzare i dati
	 
    public float damage;
    public AttackTypeEnum attackType;
    public DamageTypeEnum damageType;
    public List<GameObject> ability;

    public DamageSendProcessed() { }
    public DamageSendProcessed(float tmpDamage, AttackTypeEnum tmpAttType, DamageTypeEnum tmpDamType, List<GameObject> tmpAbility = null)
    {
        this.damage = tmpDamage;
        this.attackType = tmpAttType;
        this.damageType = tmpDamType;
        this.ability = tmpAbility;
    }
}
*/