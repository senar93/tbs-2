﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BattleObjectResource
{

	public List<BattleObjectSingleResource> resourceList;

	//aggiunge la risorsa del tipo type se non è gia presente
	public void AddResource(ResourceCostTypeEnum type, BattleObjectAbility ability, float specificValue = -1)
	{
		if (FindResource(type) < 0)
			resourceList.Add(new BattleObjectSingleResource(type, ability, specificValue));
	}

	//restituisce l'indice della risorsa se presente, altrimenti -1
	public int FindResource(ResourceCostTypeEnum type)
	{
		for (int i = 0; i < resourceList.Count; i++)
			if (resourceList[i].type == type)
				return i;

		return -1;
	}
}