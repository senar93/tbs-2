﻿[System.Serializable]
public class AttackGeneric
{
    static int MIN_DAMAGE_ALLOWED = 0;
    static int MIN_RANGE_ALLOWED = 0;
    static float MIN_ATTACK_INITIATIVE_COST_ALLOWED = 0;

    public int damageValue = 1;
    public DamageTypeEnum damageType = DamageTypeEnum.none;
    public AttackTypeEnum attackType = AttackTypeEnum.none;
    public int rangeValueMin = 0;
    public int rangeValueMax = 1;
    public float attackInitiativeCost = 1f;                 //iniziativa spesa per effettuare un attacco (utilizata anche per i calcoli del costo di molte abilità)
}