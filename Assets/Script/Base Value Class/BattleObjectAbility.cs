﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class BattleObjectAbility
{
	private GameObject objOwner;
	public List<GameObject> abilityList;
	
	//costruttore
	public BattleObjectAbility(List<GameObject> ability, GameObject objOwner, bool toOrder = false) {
		this.objOwner = objOwner;
		abilityList = ability;
		if (toOrder)
			OrderByPriority(abilityList);
	}

	//restituisce l'oggetto che contiene come parametro l'oggetto BattleObjectAbility
	public GameObject GetObjOwner() {
		return objOwner;
	}

	//DA IMPLEMENTARE
	//funzione che restituisce tutti i ResourceCostTypeEnum forniti dalle abilità (quindi controlla solo le abilità con il tag <risorsa>Base)





	//STATIC FUNCTION
	/* parte static della classe che permette di gestire le due classi di abilità (AbilityGenericScript e AbilitySpecificScript) come se fossero un unica classe
	 */
	//restituisce il valore fornito dall abilità (qualsiasi sia) passata come parametro se soddisfa il tag specificato
	public static float[] ApplyEffect(GameObject ability, GameObject[] target = null, GameObject sender = null, string optionalParameters = "")
	{
		if (ability.GetComponent<AbilityGenericScript>() != null)
		{
			return ability.GetComponent<AbilityGenericScript>().linkedScript.
				ApplyEffect(target, sender,
							ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().resourceCostScript,
							null, optionalParameters);
		}
		else if (ability.GetComponent<AbilitySpecificScript>() != null)
		{
			return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().linkedScript.
				ApplyEffect(target, sender,
							ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().resourceCostScript,
							ability.GetComponent<AbilitySpecificScript>().linkedSpecificValue, optionalParameters);
		}
		else
			return null;
	}

	//funzioni adibite a leggere i parametri generici dell abilità
	public static bool IsPassive(GameObject ability)
	{
		if (ability != null)
		{
			if (ability.GetComponent<AbilityGenericScript>() != null)
				return ability.GetComponent<AbilityGenericScript>().passive;
			else if (ability.GetComponent<AbilitySpecificScript>() != null)
				return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().passive;
		}
		return false;
	}
	public static bool IsVisible(GameObject ability)
	{
		if (ability != null)
		{
			if (ability.GetComponent<AbilityGenericScript>() != null)
				return ability.GetComponent<AbilityGenericScript>().visible;
			else if (ability.GetComponent<AbilitySpecificScript>() != null)
				return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().visible;
		}
		return false;
	}
	public static bool CanBeDisabled(GameObject ability)
	{
		if (ability != null)
		{
			if (ability.GetComponent<AbilityGenericScript>() != null)
				return ability.GetComponent<AbilityGenericScript>().canBeDisabled;
			else if (ability.GetComponent<AbilitySpecificScript>() != null)
				return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().canBeDisabled;
		}
		return false;
	}
	public static bool CanBeDispelled(GameObject ability)
	{
		if (ability != null)
		{
			if (ability.GetComponent<AbilityGenericScript>() != null)
				return ability.GetComponent<AbilityGenericScript>().canBeDispelled;
			else if (ability.GetComponent<AbilitySpecificScript>() != null)
				return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().canBeDispelled;
		}
		return false;
	}
	public static int GetPriority(GameObject ability)
	{
		if (ability != null)
		{
			if (ability.GetComponent<AbilityGenericScript>() != null)
				return ability.GetComponent<AbilityGenericScript>().priority;
			else if (ability.GetComponent<AbilitySpecificScript>() != null)
				if (ability.GetComponent<AbilitySpecificScript>().haveCustomPriorty)
					return ability.GetComponent<AbilitySpecificScript>().priority;
				else
					return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().priority;
		}
		return 0;
	}
	public static EffectApplicationTagTypeEnum GetApplicationTag(GameObject ability)
	{
		if (ability != null)
		{
			if (ability.GetComponent<AbilityGenericScript>() != null)
				return ability.GetComponent<AbilityGenericScript>().applicationTag;
			else if (ability.GetComponent<AbilitySpecificScript>() != null)
				return ability.GetComponent<AbilitySpecificScript>().linkedGenericAbility.GetComponent<AbilityGenericScript>().applicationTag;
		}
		return EffectApplicationTagTypeEnum.genericError;
	}

	//restituisce una lista di indici di abilità che contengono il tag(EffectTagTypeEnum) effectTag
	public static List<int> GetIndexByEffectTag(GameObject[] ability, EffectApplicationTagTypeEnum effectTag)
	{
		if (ability == null)
			return null;

		List<int> tmpIndex = new List<int>();
		for (int i = 0; i < ability.Length; i++)
			if (GetApplicationTag(ability[i]) == effectTag)
				tmpIndex.Add(i);

		if (tmpIndex.Count > 0)
			return tmpIndex;
		else
			return null;
	}
	public static List<int> GetIndexByEffectTag(List<GameObject> ability, EffectApplicationTagTypeEnum effectTag)
	{
		if (ability == null)
			return null;

		List<int> tmpIndex = new List<int>();
		for (int i = 0; i < ability.Count; i++)
			if (GetApplicationTag(ability[i]) == effectTag)
				tmpIndex.Add(i);

		if (tmpIndex.Count > 0)
			return tmpIndex;
		else
			return null;
	}
	// restituisce una sottolista di abilità che contengono il tag(EffectApplicationTagTypeEnum) effectApplicationTag
	public static List<GameObject> GetAbilityByeApplicationTag(List<GameObject> abilityList, EffectApplicationTagTypeEnum effectApplicationTag, bool toOrder = false)
	{
		List<GameObject> tmpList = new List<GameObject>();
		for (int i = 0; i < abilityList.Count; i++)
			if(GetApplicationTag(abilityList[i]) == effectApplicationTag)
				tmpList.Add(abilityList[i]);

		if (tmpList.Count == 0)
			return null;

		if(toOrder)
			OrderByPriority(tmpList);
		
		return tmpList;
	}

	//ordina la lista di abilità passata come parametro in ordine decrescente in base al parametro priority
	public static void OrderByPriority(List<GameObject> ability) {
		//Insertion Sort
		if (ability != null) {
			GameObject tmp;
			int j, i;
			for (i = 1; i < ability.Count; i++)
			{
				tmp = ability[i];
				for (j = i - 1; j >= 0 && (BattleObjectAbility.GetPriority(ability[j]) < BattleObjectAbility.GetPriority(tmp)); j--)
				{
					ability[j + 1] = ability[j];
				}
				ability[j + 1] = tmp;
			}
		}
	}
}