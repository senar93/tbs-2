﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[System.Serializable]
public class BattleGroupScript : BattleObject
{
	//da ogni oggetto padre eredita una funzione ConstructorNomeOggetto che setta i parametri di quell oggetto


	/* EREDITATO DA BattleObject
	 * public BattleObjectAbility ability;
	 * public GameObject owner = null;
	 */
	/* EREDITATO DA BattleObjectWithHealth
	 * public BattleObjectHealth health;
	 */


	public UnitySource baseUnity;

	public BattleObjectTagTypeEnum unityTag;
    public GameObject controlledBy = null;


	//funzione temporanea
	public void ConstructorBattleGroupScript() {
		ConstructorBattleObject(baseUnity.Get_AllAbility(true));
	}

	//funzione di test
	public void Start() {
		ConstructorBattleGroupScript();
		
		Debug.Log(health.value.Get_MaxStat());
		
	}
	

}
