﻿/* autore: Andrea Teruzzi
 * sostituisce l'Inspector di unity per lo script BaseUnityScript
 * consentendo di visualizare e modificare direttamente i parametri di particolari abilità, senza dover accederci direttamente (le modifiche si ripercuotono sull oggetto contenente l'abilità)
 */
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


[CustomEditor(typeof(BaseUnityScript))]
public class BaseUnityCustomInspectorScript : Editor {

	string lastUpdate = "28/2/2016";
	bool showStandardInspector = false;
	bool showCustomInspector = false;

	bool activeGroupHealth = true;
	bool activeGroupArmor = true;
	bool activeGroupAttack = true;
	bool activeGroupMoviment = true;
	bool showAuxiliaryList = true;
	bool showRowAbility = false;

	List<int> tmpList;


	public override void OnInspectorGUI()
	{
		#region Standard Inspector Draw
		showStandardInspector = EditorGUILayout.Foldout(showStandardInspector, "Standard Inspector");
		if (showStandardInspector)
			DrawDefaultInspector();
		#endregion

		#region Custom Inspector Draw
		showCustomInspector = EditorGUILayout.Foldout(showCustomInspector, "Custom Inspector");
		if (showCustomInspector)
		{
			EditorGUILayout.HelpBox("Last Inspector Update: " + lastUpdate,MessageType.Info);
			BaseUnityScript myTarget = (BaseUnityScript)target;
			#region Out Of Combat
			//da implementare
			#endregion
			#region Strategic Var
				//non implementate
			#endregion
			#region Health Draw
			tmpList = BattleObjectAbility.GetIndexByEffectTag(myTarget.abilityList, EffectApplicationTagTypeEnum.healthBase);
			if (tmpList != null)
			{
				activeGroupHealth = EditorGUILayout.Foldout(activeGroupHealth, "Health");
				if (activeGroupHealth)
					for (int i = 0; i < tmpList.Count; i++)
					{
						EditorGUILayout.ObjectField("Health Script", myTarget.abilityList[tmpList[i]], typeof(GameObject), false);
						myTarget.abilityList[tmpList[i]].GetComponent<Health_LinkedSpecificValueScript>().health.health = EditorGUILayout.IntField(
							"Health", (int)myTarget.abilityList[tmpList[i]].GetComponent<Health_LinkedSpecificValueScript>().health.health);
					}
			}
			#endregion
			#region Armor Draw
			tmpList = BattleObjectAbility.GetIndexByEffectTag(myTarget.abilityList, EffectApplicationTagTypeEnum.armorBase);
			if (tmpList != null)
			{
				activeGroupArmor = EditorGUILayout.Foldout(activeGroupArmor, "Armor");
				if (activeGroupArmor)
					for (int i = 0; i < tmpList.Count; i++)
					{
						EditorGUILayout.ObjectField("Armor Script", myTarget.abilityList[tmpList[i]], typeof(GameObject), false);
						myTarget.abilityList[tmpList[i]].GetComponent<Armor_LinkedSpecificValueScript>().armor.armorValue = EditorGUILayout.IntField(
							"Armor Value", (int)myTarget.abilityList[tmpList[i]].GetComponent<Armor_LinkedSpecificValueScript>().armor.armorValue);
						myTarget.abilityList[tmpList[i]].GetComponent<Armor_LinkedSpecificValueScript>().armor.armorType = (ArmorTypeEnum)EditorGUILayout.EnumPopup(
							"Armor Type", myTarget.abilityList[tmpList[i]].GetComponent<Armor_LinkedSpecificValueScript>().armor.armorType);
					}
			}
			#endregion
			#region Attack Draw
			tmpList = BattleObjectAbility.GetIndexByEffectTag(myTarget.abilityList, EffectApplicationTagTypeEnum.attackBase);
			if (tmpList != null)
			{
				activeGroupAttack = EditorGUILayout.Foldout(activeGroupAttack, "Attack");
				if (activeGroupAttack)
					for (int i = 0; i < tmpList.Count; i++)
					{
						EditorGUILayout.ObjectField("Attack Script", myTarget.abilityList[tmpList[i]], typeof(GameObject), false);
						myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.damageValue = EditorGUILayout.IntField(
							"Damage Value", (int)myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.damageValue);
						myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.damageType = (DamageTypeEnum)EditorGUILayout.EnumPopup(
							"Damage Type", myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.damageType);
						myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.attackType = (AttackTypeEnum)EditorGUILayout.EnumPopup(
							"Attack Type", myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.attackType);
						myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.rangeValueMin = EditorGUILayout.IntField(
							"Min Range", (int)myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.rangeValueMin);
						myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.rangeValueMax = EditorGUILayout.IntField(
							"Max Range", (int)myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.rangeValueMax);
						myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.attackInitiativeCost = EditorGUILayout.FloatField(
							"Initiative Cost", (float)myTarget.abilityList[tmpList[i]].GetComponent<Attack_LinkedSpecificValueScript>().attack.attackInitiativeCost);
					}
			}
			#endregion
			#region Moviment Draw
			tmpList = BattleObjectAbility.GetIndexByEffectTag(myTarget.abilityList, EffectApplicationTagTypeEnum.movimentBase);
			if (tmpList != null)
			{
				activeGroupMoviment = EditorGUILayout.Foldout(activeGroupMoviment, "Moviment");
				if (activeGroupMoviment)
					for (int i = 0; i < tmpList.Count; i++)
					{
						EditorGUILayout.ObjectField("Moviment Script", myTarget.abilityList[tmpList[i]], typeof(GameObject), false);
						myTarget.abilityList[tmpList[i]].GetComponent<Moviment_LinkedSpecificValueScript>().moviment.maxMovimentDistance = EditorGUILayout.IntField(
							"Max Distance", (int)myTarget.abilityList[tmpList[i]].GetComponent<Moviment_LinkedSpecificValueScript>().moviment.maxMovimentDistance);
						myTarget.abilityList[tmpList[i]].GetComponent<Moviment_LinkedSpecificValueScript>().moviment.movimentType = (MovimentTypeEnum)EditorGUILayout.EnumPopup(
							"Moviment Type", myTarget.abilityList[tmpList[i]].GetComponent<Moviment_LinkedSpecificValueScript>().moviment.movimentType);
						myTarget.abilityList[tmpList[i]].GetComponent<Moviment_LinkedSpecificValueScript>().moviment.movimentInitiativeCost = EditorGUILayout.FloatField(
							"Initiative Cost", (float)myTarget.abilityList[tmpList[i]].GetComponent<Moviment_LinkedSpecificValueScript>().moviment.movimentInitiativeCost);
					}
			}
			#endregion
			//ulteriori sezioni
			#region Auxiliary Draw
			showAuxiliaryList = EditorGUILayout.Foldout(showAuxiliaryList, "All Auxiliary List");
			if (showAuxiliaryList)
				for (int i = 0; i < myTarget.auxiliaryList.Length; i++)
					EditorGUILayout.ObjectField("Auxiliary " + i, myTarget.auxiliaryList[i], typeof(GameObject), false);
			#endregion

			//lista abilità filtrata
			#region All Ability Draw
			showRowAbility = EditorGUILayout.Foldout(showRowAbility, "All Ability List");
			if (showRowAbility)
				for (int i = 0; i < myTarget.abilityList.Length; i++)
					EditorGUILayout.ObjectField("Ability " + i, myTarget.abilityList[i], typeof(GameObject), false);
			#endregion
		}
		#endregion
	}

}
