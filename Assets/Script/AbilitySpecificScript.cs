﻿/* autore: Andrea Teruzzi
 */
using UnityEngine;
using System.Collections;

public class AbilitySpecificScript : MonoBehaviour{

	public GameObject linkedGenericAbility;					//punta all abilità generica che gestisce questa abilità
	public SpecificValue linkedSpecificValue;               //punta alle risorse specifiche del istanza del abilità da passare all abilità generica
	public bool haveCustomPriorty = false;
	public int priority = 0;

}
