﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public static class Checker
{
    /* classe astratta che implementa funzione che informano se un oggetto ha una certa proprietà (o è una certa cosa)
	 */
    public static bool IsAuxiliary(GameObject target)
    {
        if (target != null && target.GetComponent<BaseAuxiliaryScript>() != null)
            return true;
        return false;
    }
    public static bool IsBaseUnity(GameObject target)
    {
        if (target != null && target.GetComponent<BaseUnityScript>() != null)
            return true;
        return false;
    }
    public static bool IsGenericAbility(GameObject target)
    {
        if (target != null && target.GetComponent<AbilityGenericScript>() != null)
            return true;
        return false;
    }
    public static bool IsSpecificAbility(GameObject target)
    {
        if (target != null && target.GetComponent<AbilitySpecificScript>() != null)
            return true;
        return false;
    }
    public static bool IsAbility(GameObject target)
    {
        if (IsGenericAbility(target) || IsSpecificAbility(target))
            return true;
        return false;
    }

}