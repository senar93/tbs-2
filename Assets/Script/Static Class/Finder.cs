﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


//PROBABILMENTE DA ELIMINARE
public static class Finder
{
    /*	clase statica che implementa funzioni che restituiscono un indice in base ai parametri passati
	 */
    public static int FindAuxiliary(GameObject[] auxiliaryList, GameObject target)
    {
        if (auxiliaryList != null && target != null && Checker.IsAuxiliary(target))
            for (int i = 0; i < auxiliaryList.Length; i++)
                if (Checker.IsAuxiliary(auxiliaryList[i]) && auxiliaryList[i] == target)
                    return i;

        return -1;
    }

}