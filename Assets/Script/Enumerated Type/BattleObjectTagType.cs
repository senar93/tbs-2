﻿//indica cos è l'oggetto durante la battaglia (se un BattleGroup, oppure altro)
public enum BattleObjectTagTypeEnum
{
    custom = 0,
    battleGroup = 1,

    dispellableEffect = 2,
    notDispellableEffect = 3,

    structure = 4,
    environmentObject = 5
}