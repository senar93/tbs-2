﻿//indica la modalità di gioco
public enum GameModeEnum
{
    genericError = 0,
    tacticIperRanked = 1,
    tacticRanked = 2,
    tacticNormal = 3
}