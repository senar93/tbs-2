﻿//risorse
public enum ResourceCostTypeEnum
{
    undefined = 0,
    none = 1,

    health = 10,
    mana = 11,
    stamina = 12,

    initiative = 100
}