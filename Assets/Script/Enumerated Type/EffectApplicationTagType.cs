﻿//indica in quale momento l'abilità verrà conteggiata (per le abilità passive)
//o eventualmente quando è possibile attivare l'abilità, per le abilità passive
//se associato ad alcuni valori di EffectTagType non viene preso in considerazione
public enum EffectApplicationTagTypeEnum
{
    genericError = 0,
    
	armorBase = 11,
	attackBase = 12,
	movimentBase = 13,
	
	//health inc
	healthBase = 10,
	healthInc = 100,					//viene sommato agli hp nel calcolo
	healthIncXC = 101,					//viene moltiplicato agli hp nel calcolo
	healthIncLast = 102,                /* viene sommato agli hp nel calcolo 
										 * se per esempio si vuole ottenere un aumento del 10% della vita dell unità
										 * sarà necessario leggere la vita dell unità da dentro l'abilità e calcolare l'incremento in percentuale
										 */
	//mana
	manaBase = 20,
	manaInc = 110,
	manaIncXC = 111,
	manaIncLast = 112,

	//stamina
	staminaBase = 21,
	staminaInc = 120,
	staminaIncXC = 121,
	staminaIncLast = 122,

	none = 1
}