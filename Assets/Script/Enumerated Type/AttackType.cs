﻿//indica la modalità con cui viene effettuato l'attacco
public enum AttackTypeEnum
{
    undefined = 0,
    none = 1,

    melee = 3,
    ranged = 4,
    collateral = 5,     /* indica dei danni subiti a seguito di un colpo di cui non contano direttamente come target
						 * un danno di una mina per esempio potrebbe causare danni di un certo tipo al unità che la calpesta e danni collaterali a tutte le unità adiacenti
						 * alcune abilità potrebbero influenzare i danni collaterali e non gli altri tipi di danno o viceversa
						 */
	//da specificare ulteriormente (magie, ecc)
}