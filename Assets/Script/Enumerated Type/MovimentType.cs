﻿//indica il tipo di movimento
public enum MovimentTypeEnum
{
    undefined = 0,
    none = 1,

    walk = 10,
    fly = 11,
    swim = 12,
    teleport = 13
}