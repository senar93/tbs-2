﻿/* autore: Andrea Teruzzi
 */
using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class TEST_NetworkingPlayerScript : NetworkBehaviour {

    [SyncVar] public int x = 0;

    void Update()
    {
        if (!isLocalPlayer)
            return;
			
		if(Input.GetKeyUp(KeyCode.Space))
			Cmd_Test(1);
    }


	[Command]
	public void Cmd_Test(int n)
	{
		x +=n;
	}
}
